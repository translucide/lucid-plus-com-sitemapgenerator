<?php
/*************************************************************

 Simple site crawler to create a search engine XML Sitemap.
 Version: 2.4
 License: GPL v2
 Free to use, without any warranty.
 Written by Elmar Hanlhofer https://www.plop.at

 ChangeLog:
 ----------
 Version 2.4 2018/02/04 by Elmar Hanlhofer

     * Print configuration settings
     * Because of some confusion I moved the SITE configuration
       check down to the program start

 Version 2.3 2018/01/31 by Elmar Hanlhofer

     * Save creation date in sitemap file
     * Stop with error when user did not set the SITE variable
     * Replaced plop.at with example.com
     * Remove commented text from HTML to avoid scanning
       commented URLs

 Version 2.2 2017/10/12 by Elmar Hanlhofer

     * Cut off page anchor from URL

 Version 2.1 2016/10/07 by Elmar Hanlhofer

     * strpos fix (swap arguments) for first anchor - by William
     * First anchor check optimized - by Elmar Hanlhofer

 Version 2.0 2016/08/11 by Elmar Hanlhofer

     * Most program parts rewritten
     * Using quotes on define command
     * Supporting single and double quotes in href
     * Notices disabled

 Version 1.0 2015/10/14 by Elmar Hanlhofer

     * CLI / Website mode added
     * Multiple extension support added
     * Support for quoted URLs with spaces added
     * Skip mailto links
     * Converting special chars in the URLs for the XML file
     * Added user agent
     * Minor code updates

 Version 0.2 2013/01/16

     * curl support - by Emanuel Ulses
     * write url, then scan url - by Elmar Hanlhofer

 Version 0.1 2012/02/01 by Elmar Hanlhofer

     * Initital release

*************************************************************/
class SitemapGenerator{
    // Set the output file name.
    private $OUTPUT_FILE;

    // Set the start URL. Example: define ("SITE", "https://www.example.com");
    private $SITE;

    // Set true or false to define how the script is used.
    // true:  As CLI script.
    // false: As Website script.
    private $CLI;

    // General information for search engines how often they should crawl the page.
    private $FREQUENCY;

    // Define here the URLs to skip. All URLs that start with the defined URL
    // will be skipped too.
    // Example: "https://www.example.com/print" will also skip
    //   https://www.example.com/print/bootmanager.html
    private $SKIP_URL;

    // General information for search engines. You have to modify the code to set
    // various priority values for different pages. Currently, the default behavior
    // is that all pages have the same priority.
    private $PRIORITY;

    // When your web server does not send the Content-Type header, then set
    // this to 'true'. But I don't suggest this.
    private $IGNORE_EMPTY_CONTENT_TYPE;

    private $VERSION;
    private $NL;
    private $AGENT;
    private $SITE_SCHEME;
    private $SITE_HOST;
    private $SCANNED;
    private $STARTTIME;
    private $EXPIRED;

    public function __construct($url=null){
        $this->STARTTIME = time();
        $this->EXPIRED = false;

        $this->OUTPUT_FILE = ROOT."sitemap.xml";
        $this->SITE = ($url)?$url:substr(URL,0,-1);
        $this->CLI = true;
        $this->SKIP_URL = array();
        $this->FREQUENCY = "weekly";
        $this->PRIORITY = "0.5";
        $this->IGNORE_EMPTY_CONTENT_TYPE = false;
        $this->VERSION = "2.4";
        $this->NL = $this->CLI ? "\n" : "<br>";
        $this->AGENT = "Mozilla/5.0 (compatible; Plop PHP XML Sitemap Generator/" . $this->VERSION . ")";
        $this->SITE_SCHEME = parse_url ($this->SITE, PHP_URL_SCHEME);
        $this->SITE_HOST = parse_url ($this->SITE, PHP_URL_HOST);
        $this->SCANNED = array();
    }

    private function GetPage ($url)
    {
        $ch = curl_init ($url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_USERAGENT, $this->AGENT);

        $data = curl_exec($ch);

        curl_close($ch);

        return $data;
    }

    private function GetQuotedUrl ($str)
    {
        $quote = substr ($str, 0, 1);
        if (($quote != "\"") && ($quote != "'")) // Only process a string
        {                                        // starting with singe or
            return $str;                         // double quotes
        }

        $ret = "";
        $len = strlen ($str);
        for ($i = 1; $i < $len; $i++) // Start with 1 to skip first quote
        {
            $ch = substr ($str, $i, 1);

            if ($ch == $quote) break; // End quote reached

            $ret .= $ch;
        }

        return $ret;
    }

    private function GetHREFValue ($anchor)
    {
        $split1  = explode ("href=", $anchor);
        $split2 = explode (">", $split1[1]);
        $href_string = $split2[0];

        $first_ch = substr ($href_string, 0, 1);
        if ($first_ch == "\"" || $first_ch == "'")
        {
            $url = $this->GetQuotedUrl ($href_string);
        }
        else
        {
            $spaces_split = explode (" ", $href_string);
            $url          = $spaces_split[0];
        }
        return $url;
    }

    private function GetEffectiveURL ($url)
    {
        // Create a curl handle
        $ch = curl_init ($url);

        // Send HTTP request and follow redirections
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_USERAGENT, $this->AGENT);
        curl_exec($ch);

        // Get the last effective URL
        $effective_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        // ie. "http://example.com/show_location.php?loc=M%C3%BCnchen"

        // Decode the URL, uncoment it an use the variable if needed
        // $effective_url_decoded = curl_unescape($ch, $effective_url);
        // "http://example.com/show_location.php?loc=München"

        // Close the handle
        curl_close($ch);

        return $effective_url;
    }

    private function ValidateURL ($url_base, $url)
    {
        //Remove double //
        $url = str_replace(URL.'/',URL,$url);

        //Remove htts://sitename.com/sitename.com/...
        $url = str_replace(URL.str_replace(["https://","http://"],'',URL),URL,$url);

        //Skip URL if it contains ' or "
        if (strpos($url,'\'') !== false || strpos($url,'"') !== false) return false;

        $parsed_url = parse_url ($url);
        $scheme = $parsed_url["scheme"];

        // Skip URL if different scheme or not relative URL (skips also mailto)
        if (($scheme != $this->SITE_SCHEME) && ($scheme != "")) return false;

        $host = $parsed_url["host"];

        // Skip URL if different host
        if (($host != $this->SITE_HOST) && ($host != "")) return false;

        // Check for page anchor in url
        if ($page_anchor_pos = strpos ($url, "#"))
        {
            // Cut off page anchor
        	$url = substr ($url, 0, $page_anchor_pos);
        }

        if ($host == "")    // Handle URLs without host value
        {
            if (substr ($url, 0, 1) == '/') // Handle absolute URL
            {
                $url = $this->SITE_SCHEME . "://" . $this->SITE_HOST . $url;
            }
            else // Handle relative URL
            {
                $path = parse_url ($url_base, PHP_URL_PATH);

                if (substr ($path, -1) == '/') // URL is a directory
                {
                    // Construct full URL
                    $url = $this->SITE_SCHEME . "://" . $this->SITE_HOST . $path . $url;
                }
                else // URL is a file
                {
                    $dirname = dirname ($path);

                    // Add slashes if needed
                    if ($dirname[0] != '/')
                    {
                        $dirname = "/$dirname";
                    }

                    if (substr ($dirname, -1) != '/')
                    {
                        $dirname = "$dirname/";
                    }

                    // Construct full URL
                    $url = $this->SITE_SCHEME . "://" . $this->SITE_HOST . $dirname . $url;
                }
            }
        }
        $oldurl = $url;

        // Don't scan when already scanned
        if (in_array($url, $this->SCANNED) !== false) return false;

        // Get effective URL, follow redirected URL
        $url = $this->GetEffectiveURL ($url);

        //Look again if effective url is different than url.
        if ($oldurl != $url) {
            // Don't scan when already scanned
            if (in_array($url, $this->SCANNED) !== false) return false;
        }

        return $url;
    }

    // Skip URLs from the $skip_url array
    private function SkipURL ($url)
    {
        foreach ($this->SKIP_URL as $v)
        {
            if (substr ($url, 0, strlen ($v)) == $v) return true; // Skip this URL
        }
        return false;
    }

    private function Scan ($url)
    {
        //Do not allow more than 8h for this to complete
        if (time() - $this->STARTTIME > 3600*4) {
            $this->EXPIRED = true;
            return false;
        }

        $this->SCANNED[] = $url;  // Add URL to scanned array

        if ($this->SkipURL ($url))
        {
            echo "Skip URL $url" . $this->NL;
            return false;
        }

        // Remove unneeded slashes
        if (substr ($url, -2) == "//")
        {
            $url = substr ($url, 0, -2);
        }
        if (substr ($url, -1) == "/")
        {
            $url = substr ($url, 0, -1);
        }

        $headers = @get_headers ($url, 1);

        //Do not index pages that we cannot get headers for (server error)
        if (!$headers) return false;

        // Handle pages not found
        if (strpos ($headers[0], "404") !== false)
        {
            echo "Not found: $url" . $this->NL;
            return false;
        }

        // Handle redirected pages
        if (strpos ($headers[0], "301") !== false)
        {
            $url = $headers["Location"];     // Continue with new URL
            echo "Redirected to: $url" . $this->NL;
        }
        // Handle other codes than 200
        else if (strpos ($headers[0], "200") == false)
        {
            $url = $headers["Location"];
            echo "Skip HTTP code $headers[0]: $url" . $this->NL;
            return false;
        }

        // Get content type
        if (is_array ($headers["Content-Type"]))
        {
            $content = explode (";", $headers["Content-Type"][0]);
        }
        else
        {
            $content = explode (";", $headers["Content-Type"]);
        }

        $content_type = trim (strtolower ($content[0]));

        // Check content type for website
        if ($content_type != "text/html")
        {
            if ($content_type == "" && $this->IGNORE_EMPTY_CONTENT_TYPE)
            {
                echo "Info: Ignoring empty Content-Type." . $this->NL;
            }
            else
            {
                if ($content_type == "")
                {
                    echo "Info: Content-Type is not sent by the web server. Change " .
                         "'IGNORE_EMPTY_CONTENT_TYPE' to 'true' in the sitemap script " .
                         "to scan those pages too." . $this->NL;
                }
                else
                {
                    echo "Info: $url is not a website: $content[0]" . $this->NL;
                }
                return false;
            }
        }

        $html = $this->GetPage ($url);

        //Do not index empty pages.
        if (!$html) return false;

        $html = trim ($html);

        $html = preg_replace("/(\<\!\-\-.*\-\-\>)/sU", "", $html); // Remove commented text
        $html = str_replace ("\r", " ", $html);        // Remove newlines
        $html = str_replace ("\n", " ", $html);        // Remove newlines
        $html = str_replace ("\t", " ", $html);        // Remove tabs
        $html = str_replace ("<A ", "<a ", $html);     // <A to lowercase

        $first_anchor = strpos ($html, "<a ");    // Find first anchor

        if ($first_anchor === false) return true; // Return when no anchor found

        $html = substr ($html, $first_anchor);    // Start processing from first anchor

        $a1   = explode ("<a ", $html);

        //Extract all page links
        foreach ($a1 as $next_url)
        {
            $next_url = trim ($next_url);

            // Skip empty array entry
            if ($next_url == "") continue;

            // Get the attribute value from href
            $next_url = $this->GetHREFValue ($next_url);

            // Do all skip checks and construct full URL
            $next_url = $this->ValidateURL ($url, $next_url);

            // Skip if url is not valid
            if (!$next_url) continue;

            //Add to the toScan array...
            if ($this->Scan ($next_url))
            {
                // Add URL to sitemap
                file_put_contents ($this->OUTPUT_FILE,"  <url>\n" .
                    "    <loc>" . htmlentities ($next_url) ."</loc>\n" .
                    "    <changefreq>" . $this->FREQUENCY . "</changefreq>\n" .
                    "    <priority>" . $this->PRIORITY . "</priority>\n" .
                    "  </url>\n",FILE_APPEND
                );
            }
        }
        return true;
    }

    public function Generate() {
        if (!$this->SITE)
        {
            echo("ERROR: You did not set the SITE variable with the URL of your website!\n");
            return false;
        }

        //Delete current sitemap
        if (file_exists($this->OUTPUT_FILE)) {
            @unlink($this->OUTPUT_FILE);
        }
        $res = file_put_contents ($this->OUTPUT_FILE,
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
            "<!-- Created with Plop PHP XML Sitemap Generator " . $this->VERSION . " https://www.plop.at -->\n" .
            "<!-- Date: " . date ("Y-m-d H:i:s") . " -->\n" .
            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
            "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
            "        xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
            "        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" .
            "  <url>\n" .
            "    <loc>" . $this->SITE . "/</loc>\n" .
            "    <changefreq>" . $this->FREQUENCY . "</changefreq>\n" .
            "  </url>\n"
        );
        if (!$res)
        {
            echo("ERROR: Cannot create " . $this->OUTPUT_FILE . "!" . $this->NL);
            return false;
        }

        $this->SCANNED = array();
        $this->Scan($this->GetEffectiveURL($this->SITE));
        file_put_contents ($this->OUTPUT_FILE,"</urlset>\n",FILE_APPEND);

        if ($this->EXPIRED) {
            echo("ERROR: Sitemap generation took more than 4h. Giving up....".$this->NL);
            return false;
        }
        else return true;
    }
}
?>
