<?php
/**
 * To activate, set a cron job like this :
 * php-cli -d memory_limit=128M <pathtolucid+>/index.php --site=sitename.com --triggerevent=sys.oncronjob >/dev/null 2>&1
 */
@set_time_limit (0);
@ini_set('memory_limit', '512M');
global $service;
$service->get('EventHandler')->on('sys.onCronJob',
	function($e,$p){
        //Create data directory if not done yet.
        if (!is_dir(DATAROOT.'data/sitemapgenerator')) {
            mkdir(DATAROOT.'data/sitemapgenerator','0755',true);
        }

        //If site map is up to date or current hour is not 3am quit...
        //We only regenerate site map at 3am.
        if (!file_exists(DATAROOT.'data/sitemapgenerator/regenerate-sitemap.signal') || date('H') != 3) {
            return $p;
        }

        //Remove signal file so we dont start multiple processes simultaneously.
        unlink(DATAROOT.'data/sitemapgenerator/regenerate-sitemap.signal');

        //Processing is not done yet.
        if (file_exists(DATAROOT.'data/sitemapgenerator/regenerate-sitemap-processing.signal')) {
            return $p;
        }

        file_put_contents(DATAROOT.'data/sitemapgenerator/regenerate-sitemap-processing.signal',"1");

        //Site map must be refreshed at 3am every time a page has been saved.
		global $service;
		$service->get('Ressource')->get('com/moneyqube/model');
		$service->get('Ressource')->get('core/display/cli/textoutput');
		$service->get('Ressource')->get('core/data/collection');
		$service->get('Ressource')->get('com/sitemapgenerator/class/sitemapgenerator');

        ob_start();
        $col = new Collection();
		$c = new TextOutput();

        //Generate sitemap
        $generator = new SitemapGenerator();
        $result = $generator->Generate();

        //Send sitemap to google
        if ($result) file_get_contents('http://www.google.com/ping?sitemap='.urlencode(URL.'sitemap.xml'));

        $buffer = ob_get_contents();
        ob_end_clean();
        if (strlen($buffer) && !$result) {
            echo $c->t('generatesitemap: sys.onCronJob (com/sitemapgenerator/event/sys/oncronjob/generatesitemap)');
            echo $buffer;
        }

        //Remove signal file so we dont start multiple processes simultaneously.
        unlink(DATAROOT.'data/sitemapgenerator/regenerate-sitemap-processing.signal');

        return $p;
    }
);
?>
