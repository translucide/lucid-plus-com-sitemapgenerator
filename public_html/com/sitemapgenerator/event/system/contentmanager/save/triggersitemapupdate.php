<?php
/**
 * To activate, set a cron job like this :
 * php-cli -d memory_limit=128M <pathtolucid+>/index.php --site=sitename.com --triggerevent=sys.oncronjob >/dev/null 2>&1
 */
global $service;
$service->get('EventHandler')->on('system.contentmanager.save',
	function($e,$p){
        //Create data directory if not done yet.
        if (!is_dir(DATAROOT.'data/sitemapgenerator')) {
            mkdir(DATAROOT.'data/sitemapgenerator','0755',true);
        }

        file_put_contents(DATAROOT.'data/sitemapgenerator/regenerate-sitemap.signal','1');
        return $p;
    }
);
?>
